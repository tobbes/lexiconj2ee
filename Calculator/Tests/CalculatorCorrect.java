import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CalculatorCorrect {
private Calculator c;
	
	@Before
	public void setup() {
		c = new Calculator();
	}
	
	@Test
	public void one_plus_one_is_two() throws CalculatorException {
		
		double result = c.calculate("1+1");
		assertTrue("one plus one is not 2",result == 2);
	}

	@Test
	public void one_minus_one_is_zero() throws CalculatorException {
		double result = c.calculate("1-1");
	    assertTrue("One minus one is not zero " + result,0 == result);			
	}
	@Test (expected = CalculatorException.class)
	public void null_gets_exception() throws CalculatorException {
		
		c.calculate(null);
	}
	@Test (expected = CalculatorException.class)
	public void letters_get_exception() throws CalculatorException {
		c.calculate("abc-");
	}
	

}
