
public class Calculator {

	public double calculate(String expression) throws CalculatorException {
		
		if(expression == null)
		{
			throw new CalculatorException();
		}
		String[] subexpressions = null;
		double res = 0;
		if(expression.contains("-")){
			subexpressions = expression.split("\\-");
			try {
			res = Double.parseDouble(subexpressions[0]);
			}
			
			catch(NumberFormatException e) {
				throw new CalculatorException();
			}
			for(int i = 1; i < subexpressions.length ; i++) {
				try {
					res -= Double.parseDouble(subexpressions[i]);
				}
			catch(NumberFormatException e) {
				throw new CalculatorException();
			}
			
			}
			
		}
		
		else if(expression.contains("+")) {
		subexpressions = expression.split("\\+");
		
			for(int i = 0 ; i < subexpressions.length ; i++)
		{
			try {
				res += Double.parseDouble(subexpressions[i]);
			}
			catch(NumberFormatException e) {
				throw new CalculatorException();
			}
			}
		}
		return res;
		// TODO Auto-generated method stub
		

	}

}
